<?php

namespace Drupal\guest_lists\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GuestListController extends ControllerBase{

  public function __construct()
  {
    $config = \Drupal::config('guest_lists.settings');
    $this->table = $config->get('table');
    $this->baseUrl = $config->get('base_url');
  }

  public function index(){

    $database = \Drupal::service('database');

    $guests = [];
    $guests = $database->select($this->table, 'gl')
              ->fields('gl')
              ->execute()
              ->fetchAll();

    return array(
      '#theme' => 'guest_lists_home',
      '#lists' => $guests,
      '#title' => 'Welcome to Guest List'
    );
  }

  public function delete($guestId)
  {

    $database = \Drupal::service('database');

    $database->delete($this->table)
      ->condition('id',$guestId)
      ->execute();

    $path = \Drupal\Core\Url::fromRoute('guest_lists.index')->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }
}
