<?php

namespace Drupal\guest_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RegisterGuestForm extends FormBase{

  public function __construct() {
    $config = \Drupal::config('guest_lists.settings');

    $this->table = $config->get('table');
  }

  public function getFormId()
  {
    return 'register_guest_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#required' => true
    );

    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#required' => true
    );

    $form['gender'] = [
      '#type' => 'radios',
      '#title' => t('Gender'),
      '#default_value' => 'male',
      '#options' => [
        'male' => t('Male'),
        'female' => t('Female')
      ]
    ];

    $form['email'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => true
    );

    $form['phone'] = array(
      '#type' => 'tel',
      '#title' => t('Phone Number'),
      '#required' => true
    );

    $form['contact_type'] = array(
      '#type' => 'select',
      '#title' => t('Contact Type'),
      '#options' => [
        'mobile' => t('Mobile'),
        'home' => t('Home'),
        'office' => t('Office'),
      ],
      '#required' => true
    );

    $form['approved'] = array(
      '#type' => 'checkbox',
      '#title' => t('Approve?'),
      '#return_value' => 1
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
    );

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $fields = ['email','phone'];

    foreach($fields as $field){
      $value = $form_state->getValue($field);

      switch($field){
        case 'email':
          if(!\Drupal::service('email.validator')->isValid($value)){
            $form_state->setErrorByName($field,t('%field is not valid.',[ '%field' => $value]));
            return;
          }
          if(!\Drupal::service('guest_lists.form_validator_service')->isUnique($field, $value)) {
            $form_state->setErrorByName($field, t('This %field is already used as guest.', ['%field' => $value]));
            return;
          }
          break;
        case 'phone':
          if(!\Drupal::service('guest_lists.form_validator_service')->isUnique($field, $value)) {
            $form_state->setErrorByName($field, t('This %field is already used as guest.', ['%field' => $value]));
            return;
          }
          break;
      }
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    $connection = \Drupal::service('database');

    $result = $connection->insert($this->table)
              ->fields([
                'uid' => $user->id(),
                'first_name' => $form_state->getValue('first_name'),
                'last_name' => $form_state->getValue('last_name'),
                'email' => $form_state->getValue('email'),
                'phone' => $form_state->getValue('phone'),
                'contact_type' => $form_state->getValue('contact_type'),
                'approved' => $form_state->getValue('approved'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
              ])
              ->execute();

    \Drupal::messenger()->addStatus('New guest created.');
  }
}
