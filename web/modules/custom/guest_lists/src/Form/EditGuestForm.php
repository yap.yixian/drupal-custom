<?php

namespace Drupal\guest_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class EditGuestForm extends FormBase{

  private $guestId = null;

  public function __construct()
  {
    $config = \Drupal::config('guest_lists.settings');
    $this->table = $config->get('table');
  }

  public function getFormId()
  {
    return 'gust_list_edit_guest';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $guestId = null)
  {
    $database = \Drupal::service('database');

    $guests = $database->select($this->table,'gl')
            ->fields('gl')
            ->condition('id',$guestId)
            ->execute()
            ->fetchAll();

    if (count($guests) > 0) {
      $guest = $guests[0];
      $this->guestId = $guest->id;

      $form['first_name'] = array(
        '#type' => 'textfield',
        '#title' => t('First Name'),
        '#required' => true,
        '#default_value' => $guest->first_name
      );

      $form['last_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Last Name'),
        '#required' => true,
        '#default_value' => $guest->last_name
      );

      $form['gender'] = [
        '#type' => 'radios',
        '#title' => t('Gender'),
        '#default_value' => 'male',
        '#options' => [
          'male' => t('Male'),
          'female' => t('Female')
        ],
        '#default_value' => $guest->gender
      ];

      $form['email'] = array(
        '#type' => 'email',
        '#title' => t('Email'),
        '#required' => true,
        '#default_value' => $guest->email
      );

      $form['phone'] = array(
        '#type' => 'tel',
        '#title' => t('Phone Number'),
        '#required' => true,
        '#default_value' => $guest->phone
      );

      $form['contact_type'] = array(
        '#type' => 'select',
        '#title' => t('Contact Type'),
        '#options' => [
          'mobile' => t('Mobile'),
          'home' => t('Home'),
          'office' => t('Office'),
        ],
        '#required' => true,
        '#default_value' => $guest->contact_type
      );

      $form['approved'] = [
        '#type' => 'checkbox',
        '#title' => t('Approve?'),
        '#return_value' => 1,
        '#default_value' => $guest->approve
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Update')
      ];
    } else {
      return [
        '#markup' => 'Guest not Found.'
      ];
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $fields = ['email','phone'];

    foreach($fields as $field){
      $value = $form_state->getValue($field);

      switch($field){
        case 'email':
          if(!\Drupal::service('email.validator')->isValid($value)){
            $form_state->setErrorByName($field,t('%field is not valid.',[ '%field' => $value]));
            return;
          }
          if(!\Drupal::service('guest_lists.form_validator_service')->isUnique($field, $value)) {
            $form_state->setErrorByName($field, t('This %field is already used as guest.', ['%field' => $value]));
            return;
          }
          break;
        case 'phone':
          if(!\Drupal::service('guest_lists.form_validator_service')->isUnique($field, $value)) {
            $form_state->setErrorByName($field, t('This %field is already used as guest.', ['%field' => $value]));
            return;
          }
          break;
      }
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $connection = \Drupal::service('database');

    $result = $connection->update($this->table)
              ->fields([
                'first_name' => $form_state->getValue('first_name'),
                'last_name' => $form_state->getValue('last_name'),
                'email' => $form_state->getValue('email'),
                'phone' => $form_state->getValue('phone'),
                'contact_type' => $form_state->getValue('contact_type'),
                'approved' => $form_state->getValue('approved'),
                'updated_at' => date('Y-m-d H:i:s')
              ])
              ->condition('id', $this->guestId)
              ->execute();
    \Drupal::messenger()->addStatus('Edit success');
  }
}
