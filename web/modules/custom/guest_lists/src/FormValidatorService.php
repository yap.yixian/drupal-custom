<?php

namespace Drupal\guest_lists;

class FormValidatorService {
  private $table;

  public function __construct()
  {
    $config = \Drupal::config('guest_lists.settings');
    $this->table = $config->get('table');
  }

  public function isUnique($field, $value)
  {
    $database = \Drupal::service('database');

    $number = $database->select($this->table)
              ->condition($field, $value)
              ->countQuery()
              ->execute()
              ->fetchField();

    if($number > 0) {
      return false;
    }
    return true;
  }
}
